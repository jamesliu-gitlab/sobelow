package metadata_test

import (
	"reflect"
	"testing"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
	"gitlab.com/gitlab-org/security-products/analyzers/sobelow/v2/metadata"
)

func TestReportScanner(t *testing.T) {
	want := report.ScannerDetails{
		ID:      "sobelow",
		Name:    "Sobelow",
		Version: metadata.ScannerVersion,
		Vendor: report.Vendor{
			Name: "GitLab",
		},
		URL: "https://github.com/nccgroup/sobelow",
	}
	got := metadata.ReportScanner

	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
