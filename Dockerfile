FROM golang:1.17-alpine AS build
ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer

FROM elixir:1.11-alpine

ENV MIX_HOME=/.mix

ARG SCANNER_VERSION=0.11.1
ENV SCANNER_VERSION=$SCANNER_VERSION

ARG JASON_VERSION=1.2.2

# we must archive install jason as there is a good chance it's not available when running sobelow on older
# phoenix projects still using the older JSON dep
RUN mix local.hex --force && \
    mix archive.install --force hex sobelow $SCANNER_VERSION && \
    mix archive.install --force hex jason $JASON_VERSION && \
    mkdir /.sobelow && \
    chmod -R g+r /.sobelow

COPY --from=build /analyzer /analyzer
ENTRYPOINT []
CMD ["/analyzer", "run"]
